# VPNBOOK Download Automator

## About

The script performs a download from the vpnbook website, unpacks and replaces the line pointing to the login and password file in all .ovpn configurations, uses Tesseract OCR to scan the password image, then replaces the old password in the pass.txt file.

The script author uses Tesseract OCR with the [tessdata_best](https://github.com/tesseract-ocr/tessdata_best) model to improve the recognition.

>**NOTE:** This repository is not affiliated with the creators of VPNBook and is distributed under the GPL v3.0 license 

## How to use

>Standard program locations are prescribed in the script, replace them if necessary.

Run as administrator.

## Dependencies

* [Tesseract OCR](https://github.com/tesseract-ocr/tesseract#support)

## Support
 
The script has been tested on Win 10/11 OS builds 21H1/21H2.
