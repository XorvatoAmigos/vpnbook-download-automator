@echo off
echo Checking the Internet connection
ping -n 4 1.1.1.1 >nul
IF %ERRORLEVEL% EQU 0 (
    echo Downloading vpn configurations
    curl -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-pl226.zip -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-de4.zip -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-us1.zip -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-us2.zip -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-ca222.zip -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-ca198.zip -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-fr1.zip -OL https://www.vpnbook.com/free-openvpn-account/vpnbook-openvpn-fr8.zip
    IF %ERRORLEVEL% EQU 0 (
        
        echo Download successful
        echo Pause for 5 seconds....
        ping 127.0.0.1 -n 6 >nul
    ) ELSE (
        echo Download failed
        exit
        pause
    )
) ELSE (
    cls
    echo Lack of Internet connection
    exit
    pause
)

cls
echo Extracting vpnbook-openvpn-pl226.zip ...
tar -xvzf vpnbook-openvpn-pl226.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-pl226.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-pl226.zip extracted successfully
)

echo Extracting vpnbook-openvpn-de4.zip ...
tar -xvzf vpnbook-openvpn-de4.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-de4.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-de4.zip extracted successfully
)

echo Extracting vpnbook-openvpn-us1.zip ...
tar -xvzf vpnbook-openvpn-us1.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-us1.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-us1.zip extracted successfully
)

echo Extracting vpnbook-openvpn-us2.zip ...
tar -xvzf vpnbook-openvpn-us2.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-us2.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-us2.zip extracted successfully
)

echo Extracting vpnbook-openvpn-ca222.zip ...
tar -xvzf vpnbook-openvpn-ca222.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-ca222.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-ca222.zip extracted successfully
)

echo Extracting vpnbook-openvpn-ca198.zip ...
tar -xvzf vpnbook-openvpn-ca198.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-ca198.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-ca198.zip extracted successfully
)

echo Extracting vpnbook-openvpn-fr1.zip ...
tar -xvzf vpnbook-openvpn-fr1.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-fr1.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-fr1.zip extracted successfully
)

echo Extracting vpnbook-openvpn-fr8.zip ...
tar -xvzf vpnbook-openvpn-fr8.zip -C "C:\Program Files\OpenVPN\config"
if %ERRORLEVEL% NEQ 0 (
    echo Error extracting vpnbook-openvpn-fr8.zip
    exit
    pause
) else (
    echo vpnbook-openvpn-fr8.zip extracted successfully
)

echo Pause for 5 seconds....
ping 127.0.0.1 -n 6 >nul

cls
setlocal enabledelayedexpansion

set "search=auth-user-pass"
set "replace=auth-user-pass pass.txt"

echo Adding the string "auth-user-pass pass.txt" to all .ovpn configurations
for %%f in ("C:\Program Files\OpenVPN\config\*.ovpn") do (
    echo Processing file: %%f
    powershell -Command "(Get-Content '%%f') -replace [regex]::Escape('%search%'), '%replace%' | Set-Content '%%f'"
)

echo Replacement completed.
echo Pause for 5 seconds....
ping 127.0.0.1 -n 6 >nul

cls

set "img_url=https://www.vpnbook.com/password.php"
set "tesseract_path=C:\Program Files\Tesseract-OCR"
set "temp_image=%TEMP%\temp_image.png"
set "temp_output=%TEMP%\temp_output"
set "temp_pass=%TEMP%\temp_pass.txt"
set "permanent_pass=C:\Program Files\OpenVPN\config\pass.txt"

if not exist "%tesseract_path%" (
    echo Tesseract OCR not found at %tesseract_path%. Exiting...
    pause
    exit /b 1
)

echo Downloading image with a password
powershell -Command "(New-Object Net.WebClient).DownloadFile('%img_url%', '%temp_image%')"

echo Running Tesseract OCR on the image...
"%tesseract_path%\tesseract.exe" "%temp_image%" "%temp_output%" -l eng

echo Pause for 5 seconds....
ping 127.0.0.1 -n 6 >nul

cls
type "%temp_output%.txt"
echo Does the text match what is shown in the picture?
pause

if exist "%permanent_pass%" (
    echo Saving the first line from pass.txt to %temp_pass%
    for /F "usebackq delims=" %%i in ("%permanent_pass%") do (
        if not defined first_line (
            set "first_line=%%i"
            echo !first_line!>"%temp_pass%"
        )
    )
)

if exist "%temp_pass%" (
    set count=0
    echo Saving the first two lines from %temp_output%.txt to pass.txt
    for /F "usebackq delims=" %%a in ("%temp_output%.txt") do (
        set /A count+=1
        echo %%a>>"%temp_pass%"
        if !count! EQU 2 goto :BreakLoop
    )
    :BreakLoop
    copy /Y "%temp_pass%" "%permanent_pass%" >nul
    del "%temp_pass%"
)

echo Cleaning up temporary files...
del "%temp_image%"
del "%temp_output%.txt"

start notepad.exe "%permanent_pass%"

endlocal

echo Done!
pause
